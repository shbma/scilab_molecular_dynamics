mode(0);

exec('./constants.sce'); // константы
exec('./lib_gas.sce');   // вспомогательные функции

//[particles, cellAxes, potentAxes] = initCell2(N,L);
[particles, cellAxes] = initCell(N,sigma,L);

dt = 10^(-12);  // шаг интегрирования
nSteps = 400;   // число шагов
t = [0:dt:nSteps*dt]; // интервал времени моделирования

particles(2)(2).x = 1.01*particles(2)(2).x; // изм.нач.скорость
particles(2)(2).y = 0.99*particles(2)(2).y; // изм.нач.скорость

for it=1:length(t)
    drawlater(); //графический вывод направляем в скрытый буфер
    delete(cellAxes.children); //очистим график    
    particles = step(dt, particles);
    //particles = stepTrace(dt,particles, R1=2,scaleR=6,nSteps,it);

    drawnow();  //быстро "передергиваем" слои 
              //- копируем данные из скрытого граф.буфера в отображаемый 
end
