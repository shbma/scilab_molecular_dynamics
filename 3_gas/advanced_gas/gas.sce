mode(0);

exec('./constants.sce'); // константы
exec('./lib_gas.sce');   // вспомогательные функции

/* возвращает температуру как среднюю кинетическую энергию частиц*/
function T = getTemperature(dt, particles, L)
    N = length(particles);
    cutoff = L/2; // отсечка по разности координат
    Ek = 0;
    for i=1:N
        dx = particles(i)(2).x - particles(i)(1).x; 
        dy = particles(i)(2).y - particles(i)(1).y;

        if (abs(dx) < cutoff) && (abs(dy) < cutoff) then 
        //скочки черз край не в счет
            dr = [dx, dy];
            v = modul(dr/dt);
            Ek = Ek + 0.5*m*v^2;
        end
    end
    T = Ek / (2*kB* N);
endfunction

//[particles, cellAxes, potentAxes] = initCell2(N,L);
[particles, cellAxes] = initCell(N,sigma,L);
cellAxes.axes_bounds=[0,0,0.5,1.0]; // область графика частиц

dt = 10^(-12);  // шаг интегрирования
nSteps = 800;   // число шагов
t = [0:dt:nSteps*dt]; // интервал времени моделирования

particles(2)(2).x = 1.01*particles(2)(2).x; // изм.нач.скорость
particles(2)(2).y = 0.99*particles(2)(2).y; // изм.нач.скорость

Ts=[]; // заготовка под массив температур
temperAxes = newaxes();
temperAxes.axes_bounds=[0.5,0,0.5,1.0]; // область графика температур

for it=1:length(t)
    drawlater(); //графический вывод направляем в скрытый буфер
    
    sca(cellAxes); //работаем с частицами
    delete(cellAxes.children); //очистим график    
    particles = step(dt, particles);
    //particles = stepTrace(dt,particles, R1=2,scaleR=6,nSteps,it);

    sca(temperAxes); //работаем с температурами
    delete(temperAxes.children); //очистим график    
    Ts($+1) = getTemperature(dt, particles);    
    plot(1:it,Ts');
    xtitle("Средняя температура","шаги","К")

    drawnow();  //быстро "передергиваем" слои 
              //- копируем данные из скрытого граф.буфера в отображаемый 
end
