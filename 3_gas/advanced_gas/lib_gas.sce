//модуль вектора r
function d = modul(r)
    d = sqrt(r(1)^2 + r(2)^2);
endfunction

//потенциал Леннорда-Джонсона
function out = V(r)
    out = 4 * epsilon * (sigma^12 * r^(-12) - sigma^6 * r^(-6))
endfunction

//производная потенциала
function out=dV(r)
    dist = modul(r)
    out = -24*epsilon*sigma^6 * (2*sigma^6*dist^(-13) - dist^(-7))
endfunction

//сила действ. со стороны частицы на векторе r
function out = Force(r)
    modR = modul(r);
    out = -dV(r) * r/modR;
endfunction

/* возвращает массив стартовых положений частиц, распределенных
по квадратной сетке с шагом в sigma*/
function nests = makeNests(N,sigma,L)
    Lsquare = L-sigma;  // размер области интереса.
    margin = (L-Lsquare)/2; // отступ от границы ячейки
    
    //найдем ширину в частицах бущущей квадратной заселенной области
    n = round(sqrt(N));
    if n > Lsquare/sigma then 
        n = round(Lsquare/sigma);
    end
    nests = ones(n*n,2); // заготовка под массив положений
    
    //расставляем по квадратной сетке
    for l=0:(n-1) // строчки
        for c=1:n // колонки
            nests(c+l*n, 1) = c * sigma + margin; // x
            nests(c+l*n, 2) = l * sigma + margin; // y
        end
    end
endfunction

/*Создает набор частиц и отрисовывает их на графике.
particles - список координат частиц, 
cellAxes - ссылка на график, на котором частицы отображены
*/
function [particles,cellAxes] = initCell(N,sigma,L)
    particles = list() // контейнер для хранения координат

    // стартовые положения частиц [x1 y1; x2 y2; ...; xn yn]
    //nests = [ 0.3*L 0.3*L; 0.7*L 0.3*L; ...
    //          0.3*L 0.7*L; 0.7*L 0.7*L];
    nests = makeNests(N,sigma,L)

    // Подготовка поля графика
    scf(0); // создаем/делаем текущим графическое окно номер ноль
    clf();  // очищаем его
    cellAxes = gca(); // ссылка на объект текущих граф.осей
    cellAxes.data_bounds = [0,0;L,L];// фиксируем диапазоны для осей
    xrect(0,L,L,L); // очертим границы ячейки

    // создаем частицы
    for i=1:N
        x = nests(i,1);
        y = nests(i,2);
        particles($+1) = list( ...
        tlist(["listtype",'x','y'],  x, y), ... //частица1 шаг k-1
        tlist(["listtype",'x','y'],  x, y) ...  //частица1 шаг k
        );

        plot(particles(i)(2).x,particles(i)(2).y,".r"); // на график
    end
endfunction

/* Проверка граничных условий и пересчет координат на границах */
function r2 = boundaryTest(r) 
    r2 = r;   
    for i=1:2 
       if r(i) > L  then
         r2(i) = r2(i) - L ;        
       end        

       if r(i) < 0 then
         r2(i) = r2(i) + L; 
       end
    end
endfunction

/* Определяет самый короткий вектор между точкой particleA и 
точкой particleB, учитывая образы точки particleB в соседних 
ячейках-клонах (в рамках периодических граничных условий).
particleA,particleB = tlist(["listtype",'x','y'],  x, y)
L - характерный размер ячейки
*/
function dr = drNearest(particleA,particleB,L)
    //найдем координаты клонов частицы "B" из соседних ячеек 
    clonesB = [...
        particleB.x   particleB.y  ; // оригинал
        particleB.x-L particleB.y  ; // из ячейки слева
        particleB.x+L particleB.y  ; // из ячейки справа
        particleB.x   particleB.y+L; // из ячейки сверху
        particleB.x   particleB.y-L; // из ячейки снизу
    ];    
    
    //определим ближайшего к 'А' члена семейства 'B'    
    [value, index] = min(sqrt(...
        (clonesB(:,1)-particleA.x)^2 + ...
        (clonesB(:,2)-particleA.y)^2 ...
    ));

    //получим координаты самого короткого вектора BA
    dr = [particleA.x - clonesB(index,1); ...
          particleA.y - clonesB(index,2)]
endfunction


/* Вычисляет координаты частиц спустя dt от текущего момента */
function particles2 = step(dt,particles)
    N = length(particles);
    //перебор по частицам
    for a=1:N 
        // возьмем r на прошлом и позапрошлом шаге
        r_minus1 = [particles(a)(1).x; particles(a)(1).y];
        r = [particles(a)(2).x; particles(a)(2).y];

        // найдем равнодействующую силу
        multiForse = [0;0]; // вектор-заготовка
        //перебор по окружающим частицу "а" частицам
        for i=1:N     
            if i <> a then  // сама с собой не вз-действует   
                dr = drNearest(particles(a)(2),particles(i)(1),L);
                if (dr < 2*sigma) then //слишком далекие не учитываем
                    multiForse = multiForse + Force(dr);           
                end
            end
        end 

        //формула Верле для новых координат
        newCoords = 2*r - r_minus1 + 0.5*multiForse*(dt^2)/m;
        newCoords = boundaryTest(newCoords);

        //сдвигаем информацию в массиве частиц
        particles(a)(1) = particles(a)(2);    
        //обновляем последнее значение
        particles(a)(2).x = newCoords(1);
        particles(a)(2).y = newCoords(2);  

        //Рисуем активную ("a-тую") частицу
        plot(particles(a)(2).x,particles(a)(2).y,'.b'); 
        
        particles2 = particles;   
    end
endfunction






// ===================== в дополнение =====================


/*Создает набор частиц и отрисовывает их на графике.
В отличие от initCell отображает на графике еще и потенциал
взаимодействия и текущую точку на нем.
particles - список координат частиц, 
cellAxes - ссылка на график, на котором частицы отображены
potentAxes - ссылка на график с потенциалом вз-действия.
*/
function [particles,cellAxes,potentAxes] = initCell2(N,L)
    particles = list() // контейнер для хранения координат

    // стартовые положения частиц [x1 y1; x2 y2; ...; xn yn]
    nests = [ 0.3*L 0.5*L; 0.7*L 0.5*L];

    // Подготовка поля графика
    scf(0); // создаем/делаем текущим графическое окно номер ноль
    clf();  // очищаем его
    cellAxes = gca(); // ссылка на объект текущих граф.осей
    cellAxes.data_bounds = [0,0;L,L];// фиксируем диапазоны для осей
    xrect(0,L,L,L); // очертим границы ячейки

    // создаем частицы
    for i=1:N
        x = nests(i,1);
        y = nests(i,2);
        particles($+1) = list( ...
        tlist(["listtype",'x','y'],  x, y), ... //частица1 шаг k-1
        tlist(["listtype",'x','y'],  x, y) ...  //частица1 шаг k
        );

        plot(particles(i)(2).x,particles(i)(2).y,".r"); // на график
    end
    
    // Покажем текущий уровень потенциала взаимодействия
    d = modul([particles(1)(2).x - particles(2)(2).x, ...
    particles(1)(2).y - particles(2)(2).y]);
    //xsetech([0.6 0.0 0.4 0.8]); // выделим зону под график
    xsetech([0.2 0.0 0.5 0.4]); // выделим зону под график
    potentAxes = gca();
    q = sigma*linspace(0.98,2,50); // взяли подходящий диапазон
    plot(q,V(q)) // рисуем
    title("Текущий потенциал взаимодействия")
    plot([d],V(d),'xr') // отметим точку текущего потенциала

endfunction


/* Вычисляет координаты частиц спустя dt от текущего момента.
В отличие от step отрисовывает график и точку на потенциале 
взаимодействия */
function particles2 = step2(dt, particles, cellAxes, potentAxes)
    N = length(particles);
    //перебор по частицам
    for a=1:N 
        // возьмем r на прошлом и позапрошлом шаге
        r_minus1 = [particles(a)(1).x; particles(a)(1).y];
        r = [particles(a)(2).x; particles(a)(2).y];

        // найдем равнодействующую силу
        multiForse = [0;0]; // вектор-заготовка
        //перебор по окружающим частицу "а" частицам
        for i=1:N     
            if i <> a then  // сама с собой не вз-действует   
                dr = drNearest(particles(a)(2),particles(i)(1),L);
                multiForse = multiForse + Force(dr);           
            end
        end 

        //формула Верле для новых координат
        newCoords = 2*r - r_minus1 + 0.5*multiForse*(dt^2)/m;

        //сдвигаем информацию в массиве частиц
        particles(a)(1) = particles(a)(2);    
        //обновляем последнее значение
        particles(a)(2).x = newCoords(1);
        particles(a)(2).y = newCoords(2);  

        //Рисуем активную ("a-тую") частицу
        sca(cellAxes)
        plot(particles(a)(2).x,particles(a)(2).y,'.b'); 
        
        // Покажем текущий уровень потенциала взаимодействия
        sca(potentAxes);
        d = modul([particles(1)(2).x - particles(2)(2).x, ...
        particles(1)(2).y - particles(2)(2).y]);
        q = sigma*linspace(0.98,2,50); // взяли подходящий диапазон
        plot(q,V(q)) // рисуем
        title("Текущий потенциал взаимодействия")
        plot([d],V(d),'xr') // отметим точку текущего потенциала
        
        particles2 = particles;   
    end
endfunction

/* Вычисляет координаты частиц спустя dt от текущего момента.
Отличатся от функции step() тем, что
радиус точки на графике увеличивается с каждым шагом.
R1 - начальный радиус, scaleR - во сколько раз в итоге изменится,
nSteps - сколько всего шагов в интервале, it - номер текущего шага.*/
function particles2 = stepTrace(dt,particles, R1,scaleR,nSteps,it)
    N = length(particles);
    //перебор по частицам
    for a=1:N 
        // возьмем r на прошлом и позапрошлом шаге
        r_minus1 = [particles(a)(1).x; particles(a)(1).y];
        r = [particles(a)(2).x; particles(a)(2).y];

        // найдем равнодействующую силу
        multiForse = [0;0]; // вектор-заготовка
        //перебор по окружающим частицу "а" частицам
        for i=1:N     
            if i <> a then  // сама с собой не вз-действует   
                //dr = r - [particles(i)(1).x; particles(i)(1).y];
                dr = drNearest(particles(a)(2),particles(i)(1),L);
                multiForse = multiForse + Force(dr);           
            end
        end 

        //формула Верле для новых координат
        newCoords = 2*r - r_minus1 + 0.5*multiForse*(dt^2)/m;
        newCoords = boundaryTest(newCoords);

        //сдвигаем информацию в массиве частиц
        particles(a)(1) = particles(a)(2);    
        //обновляем последнее значение
        particles(a)(2).x = newCoords(1);
        particles(a)(2).y = newCoords(2);  

        //Рисуем активную ("a-тую") частицу
        plot(particles(a)(2).x,particles(a)(2).y,'or'); 
        dot = gca().children.children(1);
        dot.mark_background = 1;
        //изменяем размер частицы на графике
        if scaleR >=1 then
            newR = round(R1 * scaleR * it / nSteps);
        else
            newR = round(R1 * (scaleR + 1 - it / nSteps));
        end
        dot.mark_size = newR;
        //mprintf("size = %e\n", newR);
        
        particles2 = particles;   
    end
endfunction
