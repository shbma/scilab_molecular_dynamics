clf();
mode(0); // разрешаем вывод в консоль

// Исходные данные и физические постоянные
g = 9.81; 
v0 = 25;  // начальная скорость
a = %pi/4;  // угол броска
h0 = 1.5; // высота начала полета

/* Система четырех ДУ динамики брошенного тела в вакууме */
function dVector = fallingVacuum(t,vector) 
    dVector = zeros(5,1); // заготовка под результат
    
    dVector(1) = -g;         // dvy/dt = g
    dVector(2) = vector(1);  // dy/dt = vy 
    dVector(3) = 0;          // dvx/dt = -1
    dVector(4) = vector(3);  // dx/dt = vx 
endfunction

// Начальные условия
y0 = [v0*sin(a);h0; v0*cos(a);0]; // начальные vy, y, vx, x
t0 = 0;            // начальный момент времени
t = 0:0.1:3.6;     // время движения

flyStone = ode("rk", y0,t0,t,fallingVacuum); // решение ДУ 

//plot(flyStone(4,:),flyStone(2,:),"r"); // график y(x)

/* Аналитическое решение для системы ДУ брошенного камня */
function [vy,y,vx,x] = analitFalling(t,x0,y0,v0,alpha)  
    vy0 = v0*sin(alpha);
    vx0 = v0*cos(alpha);
    
    vy = vy0 - g*t;
    y = y0 + vy0*t - 0.5*g*t^2;
    vx = vx0;
    x = x0 + vx0*t;
endfunction

// получаем Аналитическое решение
[anVy,anY,anVx,anX] = analitFalling(t,0,h0,v0,a);

// Полная энергия
T = 0.5*(flyStone(1,:)^2+flyStone(3,:)^2); // кинетическая
U = g*flyStone(2,:); // потенциальная
Energy =  T + U;

// траектория полета
subplot(2,1,1);
plot(flyStone(4,:),flyStone(2,:),"r",anX,anY,"o"); 

// полная энергия и ее компоненты
subplot(2,1,2);
plot(t,T,"g", t,U,"b", t,Energy,"r"); 


//scf(0); clf();
